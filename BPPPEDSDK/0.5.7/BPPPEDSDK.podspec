#
#  Be sure to run `pod spec lint PEDSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|
  s.name         = "BPPPEDSDK"
  s.version      = "0.5.7"
  s.summary      = "Connect to multiple PED suppliers in a standard way."
  s.description  = <<-DESC
This is a PED SDK for Brightpearl payments. Currently it supports the following PED types:

* Developer (Fake) PED
* Paypal Here
* iZettle (Note: not supported by Brightpearl Payments)

It allows you to:

* Activate a PED using a payment method ID
* Take a payment using PED
* Save that payment against an sales order in a way that facilitates Brightpearl Payment's back office processes
* Perform a refund against a sales credit   
                   DESC

  s.homepage     = "https://shuttleglobal.com"
  s.license      = { :type => 'Apache License, Version 2.0', :text => <<-LICENSE
      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at
      
      http://www.apache.org/licenses/LICENSE-2.0
      
      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
      LICENSE
  }

  s.author             = { "Shuttle Global" => "support@shuttleglobal.com" }
  s.platform     = :ios, "12.2"

  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.

  s.source       = { :git => "https://bitbucket.org/paywithbolt/bpppedsdk.git", :tag => "0.5.7" }
  s.source_files  = "BPPPEDSDK/**/*.{h,m}"
  s.exclude_files = "Classes/Exclude"
  s.frameworks = "UIKit"
  s.requires_arc = true

  s.pod_target_xcconfig = {
      'ARCHS' => 'arm64 x86_64',
      'ONLY_ACTIVE_ARCH' => 'YES',
      'ENABLE_BITCODE' => 'NO',
      'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64', # This is required to get around Paypal Bundling Issue, may cause issues with building on new M1 Macs
      'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES'
  }
  
  s.dependency 'PEDSDK', '0.5.7'
end
