#
#  Be sure to run `pod spec lint PEDSDK.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|
  s.name          = "PEDSDK"
  s.version       = "0.6.4"
  s.summary       = "Connect to multiple PED suppliers in a standard way."
  s.description   = <<-DESC
  Connect to multiple PED suppliers in a standard way.
  Support: iZettle, PayPalHereSDKv2, Adyen, Moneris, USAEPAY
                   DESC
  s.homepage      = "http://shuttleglobal.com"
  s.license      = { :type => 'Apache License, Version 2.0', :text => <<-LICENSE
      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at
      
      http://www.apache.org/licenses/LICENSE-2.0
      
      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
      LICENSE
  }
  s.author        = { "Shuttle Global" => "support@shuttleglobal.com" }
  s.platform      = :ios, "12.2"
  s.source        = { :git => "https://bitbucket.org/paywithbolt/pedsdk.git", :tag => "0.6.4" }
  s.source_files  = [ "SquareReaderSDK.xcframework", "PEDSDK/**/*.{h,m}" ]
  s.exclude_files = "Classes/Exclude"
  s.frameworks    = "UIKit"
  s.vendored_frameworks = "SquareReaderSDK.xcframework"
  s.requires_arc  = true
  s.pod_target_xcconfig = {
      'ONLY_ACTIVE_ARCH' => 'YES'
  }

  s.dependency 'PayPalHereSDKv2/Release', '2.4.0021363000'
  s.dependency 'iZettleSDK', '3.7.0'  
end
